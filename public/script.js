let numSquares = 6;
let colors = [];
let pickedColor;

let squares = document.querySelectorAll('.square');
let colorDisplay = document.querySelector('#colorDisplay');
let messageDisplay = document.querySelector('#message');
let h1 = document.querySelector('h1');
let resetBtn = document.querySelector('#reset');
let modeBtns = document.querySelectorAll('.mode');

init();

function init() {
    setUpModeButtons();
    setUpSquares();
    resetGame();
}

function setUpModeButtons() {
    for (let i = 0; i < modeBtns.length; i++) {
        modeBtns[i].addEventListener('click', function() {
            modeBtns[0].classList.remove('selected');
            modeBtns[1].classList.remove('selected');
            this.classList.add('selected');
            if (this.textContent === 'Easy') {
                numSquares = 3;
            } else {
                numSquares = 6;
            }
            resetGame();
        });
    }
}

function setUpSquares() {
    for (let i = 0; i < squares.length; i++) {
        squares[i].addEventListener('click', function() {
            if (this.style.backgroundColor === pickedColor) {
                messageDisplay.textContent = 'Correct!';
                h1.style.backgroundColor = pickedColor;
                resetBtn.textContent = 'Play Again?';
                changeColor(pickedColor);
            } else {
                this.style.backgroundColor = '#232323';
                messageDisplay.textContent = 'Try Again';
            }
        });
    }
}

resetBtn.addEventListener('click', function() {
    resetGame();
});

function changeColor(color) {
    for (let i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    }
}

function pickColor() {
    let random = Math.floor(Math.random() * colors.length);
    return colors[random];
}

function generateRandomColors(num) {
    let colors = [];
    for (let i = 0; i < num; i++) {
        colors.push(randomColor());
    }
    return colors;
}

function randomColor() {
    let red = Math.floor(Math.random() * 256);
    let blue = Math.floor(Math.random() * 256);
    let green = Math.floor(Math.random() * 256);
    return `rgb(${red}, ${blue}, ${green})`;
}

function resetGame() {
    colors = generateRandomColors(numSquares);
    pickedColor = pickColor();
    colorDisplay.textContent = pickedColor;
    for (let i = 0; i < squares.length; i++) {
        if (colors[i]) {
            squares[i].style.backgroundColor = colors[i];
            squares[i].style.display = 'block';
        } else {
            squares[i].style.display = 'none';
        }
    }
    h1.style.backgroundColor = 'steelblue';
    messageDisplay.textContent = '';
    resetBtn.textContent = 'New Colors';
}
