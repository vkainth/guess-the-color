# Guess the Color

[https://www.vkainth.gitlab.io/guess-the-color](https://www.vkainth.gitlab.io/guess-the-color)

## Introduction

A simple application built with vanilla HTML, CSS, and JavaScript that helps
users understand rgb colors in a fun and productive way.

## Installation

- Clone the repo

- Open the downloaded repo in your file browser

- Open `index.html` in your browser

## Deploying

- Use `.gitlab-ci.yml` to create your own pipeline
